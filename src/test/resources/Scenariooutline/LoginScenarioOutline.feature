Feature: Validating The Demo Web Shop 

Scenario Outline: Verifying the login functionality in the demo web shop
Given Open the demo web shop website
Then The demo web shop home page will displayed
When Click the login button
And Enter the email "<Username>" in the email text field
And Enter the password "<Password>" in the password text field
And Click on the login button
Then The home page with loged user name should display
And Click the logout button
Then the user should taken to the homepage of the demo web shop
And close the browser

Examples:
    | Username               | Password |
    | shopdemo1223@gmail.com | Demo@123 |
    | shopdemo1223@gmail.com | Demo@123 |
    